public class Fibonacci{
 public static void main(String args[]){
   
   int i = 0, a = 0, b = 1, c = 0;

   System.out.println("Serie de numeros Fibonacci con for: ");
   for (i = 0; i < 10; i++){
    if (i < 9){
     System.out.println(a + ",");
     c = a + b;
     a = b;
     b = c;
    } else{
     System.out.print(a);
    }
   }
   System.out.println("");
   i = 0;
   a = 0;
   b = 1;
   c = 0;
   System.out.println("Serie de numeros Fibonacci con while: ");
   while(i < 10){

    if (i < 10){
     System.out.println(a + ",");
     c = a + b;
     a = b;
     b = c;
    } else{
     System.out.print(a);
    }
    i++;
  }
   i = 0;
   a = 0;
   b = 1;
   c = 0;
   System.out.println("Serie de numeros Fibonacci con do-while: ");
    
   do{ 
     if (i < 9){
     System.out.println(a + ",");
     c = a + b;
     a = b;
     b = c;
    } else{
     System.out.print(a);
    }
    i++;
    } while(i < 10);
   
   
 }
}