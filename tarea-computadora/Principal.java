import javax.swing.JOptionPane;

public class Principal{
	
  public static void main(String args[]){
  
  int option;
  int pantalla  = 0;
  String modelo = "";
  String placa  = "";
  String estado = "";
  Computadora computadora = null;
  
  do{
   option = Integer.parseInt(JOptionPane.showInputDialog(" ********************************bienvenido******************************** "
  
														+"\n1. Crear instancia de computadora sin valores."
														+"\n2. Crear instancia de computadora con los valores necesarios para cada uno de sus atributos."
														+"\n3. Modificar la placa de la computadora."
														+"\n4. Modificar el modelo de la computadora."
														+"\n5. Modificar el tamaño de la pantalla de la computadora."
														+"\n6. Modificar el estado de la computadora."
														+"\n7. Ver los datos de la computadora."
														+"\n8. Salir "));
										
    switch (option){
		
		case 1:		      
		   if(computadora == null){
			   
		       computadora = new Computadora();  				   
			}else{
				JOptionPane.showMessageDialog(null, "ya existe una computadora registrada, si desea registrar una nueva y eliminar la anterior, reinicie el programa");
				
				}
			break;
			
		case 2:		      
		        if(computadora == null){
			   
		       
					pantalla  = Integer.parseInt(JOptionPane.showInputDialog("Digite el tamaño de la pantalla"));
					modelo    = JOptionPane.showInputDialog("Digite el modelo de la computadora");
					placa     = JOptionPane.showInputDialog("Ingrese la placa de la computadora");
				
			        computadora = new Computadora(modelo, placa, pantalla);
			        JOptionPane.showMessageDialog(null, "Se ha registrado la computadora con éxito");
			          				   
			}else{
				JOptionPane.showMessageDialog(null, "ya existe una computadora registrada, si desea registrar una nueva y eliminar la anterior, reinicie el programa");
				
				}
			break;
			
		case 3:		      
		         if(computadora != null){
				   
				   placa = JOptionPane.showInputDialog("Defina el número de placa de la computadora");
				   computadora.setPlaca(placa);
				 
				   JOptionPane.showMessageDialog(null, "Se ha modificado la placa con éxito a " + placa);
			   } else {
				   JOptionPane.showMessageDialog(null, "No se ha podido modificar porque no se ha registrado ninguna computadora");
				}//fin if/else
			break;
			
		case 4:		      
		         if(computadora != null){
				   
				   modelo = JOptionPane.showInputDialog("Ingrese el modelo de la computadora");
				   computadora.setModelo(modelo);
				 
				   JOptionPane.showMessageDialog(null, "Se ha modificado el modelo con éxito a " + modelo);
			   } else {
				   JOptionPane.showMessageDialog(null, "No se ha podido modificar porque no se ha registrado ninguna computadora");
				}//fin if/else 
			  
			break;
			
		case 5:		      
		         if(computadora != null){
				   
				   pantalla = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el tamaño de la pantalla de la computadora"));
				   computadora.setPantalla(pantalla);
				 
				   JOptionPane.showMessageDialog(null, "Se ha modificado el tamaño de la pantalla con éxito a " + pantalla);
			   } else {
				   JOptionPane.showMessageDialog(null, "No se ha podido modificar porque no se ha registrado ninguna computadora");
				}//fin if/else   
			
			break;
			
		case 6:		      
		        if(computadora != null){
				   
				   estado = JOptionPane.showInputDialog("Ingrese el estado de la computadora");
				   computadora.setEstado(estado);
				 
				   JOptionPane.showMessageDialog(null, "Se ha modificado el estado con éxito a " + estado);
			   } else {
				   JOptionPane.showMessageDialog(null, "No se ha podido modificar porque no se ha registrado ninguna computadora");
				}//fin if/else   
			
			break;
			
		case 7:		
		        if(computadora != null){
					JOptionPane.showMessageDialog(null, computadora.toString());
					}else{
		            JOptionPane.showMessageDialog(null, "No se ha podido modificar porque no se ha registrado ninguna computadora");
			}//fin if/else
			break;
			
		case 8:		      
		      
			
			break;
		
		}//switch
    } while (option != 8);//fin Do While
  }//main

}//class
